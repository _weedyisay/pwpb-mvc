<?php

require_once("../app/core/Controller.php");

class Login extends Controller {
    public function index($msg=''){
        $data['title']= 'Login';
        $data['data'] = 'Ini Sudah Halaman Login';
        $data['msg'] = $msg;
        $this->view('login/signup', $data);
    }
    public function signin(){
        $data['title']= 'Login';
        $data['data'] = 'Ini Sudah Halaman Login';
        $data['msg'] = '';
        $this->view('login/signin', $data);
    }
    public function storeLogin(){
        if($this->model('Auth')->findUserByEmailorUsername($_POST['userMail'], $_POST['userMail'])){
            $loggedInUser = $this->model('Auth')->login($_POST);
            if($loggedInUser){
                Flasher::setFlash('Login Success', 'Login', 'success');
                ($this->model('Auth')->createSession($loggedInUser));
            } else {
                Flasher::setFlash('Login Success', 'Login', 'success');
                $data['title'] = 'Login';
                $data['msg'] = 'Password Incorrect';
                $this->view('login/signin', $data);
            }
        } else {
            $data['title'] = 'Login';
            $data['msg'] = 'Username or Email not found';
            $this->view('login/signin', $data);
        }
    }
    public function store($msg = ''){
        if(isset($_POST['password']) && $_POST['password'] == $_POST['konfirm_password']){
            
            if($this->model('Auth')->register($_POST) > 0){
                redirect("/login/signin");
            }
        } else {
            $data['msg'] = 'Password must be match!';
            $this->view('login/signup', $data);
        } 
    }

    public function logout(){
        $_SESSION = [];
        redirect('login/');
    }



    
}


?>
