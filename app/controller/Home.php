<?php
require_once("../app/core/Controller.php");

class Home extends Controller {
    public function index(){
        $data['title']='Home';
        $data['users'] = $this->model('User_model')->getAllUser();
        $this->view("templates/header",$data);
        $this->view("home/index",$data);
        $this->view("templates/footer",$data);

    }

    public function about($company = "SMKN1") {
        $data['title'] = "About";
        $data['company'] = $company;
        $data['user'] = $this->model('User_model')->getAllUser();
        $this->view('templates/header', $data);
        $this->view('home/about', $data);
        $this->view('templates/footer', $data);
    }

    public function show($id){
        $data['users'] = $this->model('User_model')->getAllUser();
        $data['user'] = $this->model('User_model')->getUserById($id);
        $this->view('templates/header', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer', $data);
    }

    public function create(){
        if($this->model('User_model')->createUser($_POST) > 0){
            Flasher::setFlash('User added', 'Create User', 'success');
            redirect('/home/about');
        } else {
            echo("eror pak");
        }
    }

    public function profile(){
        $data['title'] = 'Profile';
        
        $this->view('templates/header', $data);
        $this->view('home/profile', $data);
        $this->view('templates/footer', $data);
    }

    
}