<?php

class Auth {
    private $table = 'users';
    private $db;

   
    public function __construct(){
        $this->db = new Database;
    }

    public function findUserByEmailorUsername($username, $email){
        $this->db->query("SELECT * FROM {$this->table} WHERE `username` =:username or `email` =:email ");
        $this->db->bind('username', $username);
        $this->db->bind('email', $email);
        $row = $this->db->resultSingle();

        if($this->db->rowCount() > 0){
            return $row;
        } else {
            return false;
        }
    }
    public function register($data)
    {
        $username = htmlspecialchars($data['username']);
        $first_name = htmlspecialchars($data['first_name']);
        $last_name = htmlspecialchars($data['last_name']);
        $email = htmlspecialchars($data['email']);
        $hashedPass = password_hash($data['password'], PASSWORD_DEFAULT);

        if ($this->findUserByEmailorUsername($username, $email) > 0) {
            Flasher::setFlash('Email atau username sudah ada', 'Register', 'danger');
            redirect('login/index');
        } else {
            $this->db->query("INSERT INTO {$this->table} (`username`,`first_name`,`last_name`,`email`,`password`) VALUES (:username, :first_name, :last_name, :email, :password)");
            $this->db->bind(":username", $username);
            $this->db->bind(":first_name", $first_name);
            $this->db->bind(":last_name", $last_name);
            $this->db->bind(":email", $email);
            $this->db->bind(":password", $hashedPass);
            $this->db->execute();

            return $this->db->rowCount();

        }

    }

    public function login($data){
        $userMail = htmlspecialchars($data['userMail']);
        $row = $this->findUserByEmailorUsername($userMail, $userMail);
        if( $row == false) return false;
        $hashedPass = $row['password'];
        
        if(password_verify($data['password'], $hashedPass)){
            return $row;
        } else {
            return false;
        }

    }

    public function createSession($user){
        $_SESSION['user_id'] = $user['id'];
        $_SESSION['username'] = $user['username'];
        $_SESSION['first_name'] = $user['first_name'];
        $_SESSION['last_name'] = $user['last_name'];
        $_SESSION['email'] = $user['email'];

        header('Location: ' . BASE_URL . '/home/index');
    }


}



