<?php

class User_model {
    private $table = 'user';
    private $db;

    private $name = 'Wisnu';
    public function getUser(){
        return $this->name;
    }

    public function __construct(){
        $this->db = new Database;
    }

    public function register(){
        
        $query = "SELECT * FROM {$this->table} ";
    }

    public function getAllUser(){
        $this->db->query("SELECT * FROM {$this->table}");
        return $this->db->resultAll();
    }
    public function getUserById($id){
        $this->db->query("SELECT * FROM {$this->table} WHERE `id`=:id");
        $this->db->bind('id', $id);
        return $this->db->resultSingle();
    }

    public function createUser($data){
        $hash = password_hash($data['password'], PASSWORD_DEFAULT);
        $query = "INSERT INTO {$this->table} (`first_name`,`last_name`,`email`,`password`) 
                    VALUES 
                        (:first_name, :last_name, :email, :password)";
        $this->db->query($query);
        $this->db->bind('first_name', $data['first_name']);
        $this->db->bind('last_name', $data['last_name']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('password', $hash);
        
        $this->db->execute();
        return $this->db->rowCount();
    }
}


?>
