<?php

 

class App {
    protected   $controller = "Home",
                $method     = "index",
                $params     = [];

    public function __construct(){
        $url = $this->parseUrl();
        if(!isset($_SESSION['username']) && ucwords($url[0]) != 'Login' ){
            require_once("../app/controller/Login.php");
            $this->controller = "Login";
            $this->controller = new $this->controller;
            $this->method = "index";
            unset($url[0]);
            unset($url[1]);
            if(!empty($url)){
                $this->params = array_values($url);
            }
            call_user_func_array([$this->controller, $this->method], $this->params);

        }  elseif(isset($_SESSION['username']) && ucwords($url[0]) == 'Login' ) {
            redirect('home/index');
        } else {

            if(isset($url[0]) && file_exists("../app/controller/" . ucwords($url[0]).".php")){
                $this->controller = $url[0];
                unset($url[0]);
            } else {
                require_once("../app/controller/Home.php");
            }
            require_once("../app/controller/" .$this->controller.".php");
            $this->controller = new $this->controller;

            if(isset($url[1]) && method_exists($this->controller, $url[1])){
                $this->method = $url[1];
                unset($url[1]);
            } 

            
            if(!empty($url)){
                $this->params = array_values($url);
                
            }
            call_user_func_array([$this->controller, $this->method], $this->params);

            }

    }
    public function parseUrl(){

        if(isset($_GET["url"])){
            $url = rtrim($_GET["url"],"/");
            $url = filter_var($url,FILTER_SANITIZE_URL);
            $urls = explode("/", $url);
            return $urls;
        } else {
            return [$this->controller];
        }
        
        // if(isset($_GET["url"])){
        //     return $_GET["url"];
        // } else {
        //     return "home";
        // };
    } 
}


?>

