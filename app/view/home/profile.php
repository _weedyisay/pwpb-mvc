<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Profile</h1>
 
      </div>

    

      <h2>Selamat datang, <?= !isset($data['nama']) ? $_SESSION['username'] : $data['nama']  ?>. Berikut data Anda</h2>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Username</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">*</th>
            <td><?= $_SESSION['username'] ?></td>
            <td><?= $_SESSION['first_name'] ?></td>
            <td><?= $_SESSION['last_name'] ?></td>
            <td><?= $_SESSION['email'] ?></td>
          </tr>
        </tbody>
      </table>
    </main>