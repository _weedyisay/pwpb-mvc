<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">About</h1>
 
      </div>

      <div class="d-flex flex-grow-1 justify-content-end">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Tambah Siswa
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="<?=BASE_URL?>/home/create" method="post" class="">
                            <div class="">
                                <label for="first_name" class="form-label" >First Name</label>
                                <input class="form-control" type="text" id="first_name" name="first_name">
                            </div>
                            <div class="">
                                <label for="last_name" class="form-label">Last Name</label>
                                <input class="form-control" type="text" id="last_name" name="last_name">
                            </div>
                            <div class="">
                                <label for="email" class="form-label">Email</label>
                                <input class="form-control" type="email" id="email" name="email">
                            </div>
                            <div class="">
                                <label for="password" class="form-label">password</label>
                                <input class="form-control" type="password" id="password" name="password">
                            </div>
                            
                            
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>    
    </div>

      <h2>Selamat datang, <?= !isset($data['nama']) ? "Anonimus" : $data['nama']  ?></h2>
      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col">Email</th>
              <th scope="col">Action</th>

            </tr>
          </thead>
          <tbody>
            <?php $i = 1?>
            <?php foreach ($data['user'] as $key => $value) { ?>
              <tr>
                <td><?= $i++ ?></td>
                <td><?= $value['first_name'] ?></td>
                <td><?= $value['last_name'] ?></td>
                <td><?= $value['email'] ?></td>
                <td><a href="<?= BASE_URL ?>/home/show/<?=$value['id']?>" class="btn btn-info text-white me-2">Lihat</a><a href="<?=BASE_URL?>/edit?id=<?= $value["id"] ?>" class="btn btn-warning text-white me-2">Edit</a><a href="#" class="btn btn-danger">Hapus</a></td>

              </tr>
            <?php }?>
          </tbody>
        </table>
      </div>
    </main>