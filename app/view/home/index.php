<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
  <div class="row">
    <div class="col-lg-6">
      <?php Flasher::flash(); ?>
    </div>
  </div>

      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group me-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
          </div>
          <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar" class="align-text-bottom"></span>
            This week
          </button>
        </div>
      </div>

      <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
      

      <h2>Selamat datang, <?= !isset($data['nama']) ? $_SESSION['username'] : $data['nama']?></h2>
      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col">Email</th>
              <th scope="col">Action</th>

            </tr>
          </thead>
          <tbody>
            <?php $i = 1?>
            <?php foreach ($data['users'] as $key => $value) { ?>
              <tr>
                <td><?= $i++ ?></td>
                <td><?= $value['username'] ?></td>
                <td><?= $value['first_name'] ?></td>
                <td><?= $value['last_name'] ?></td>
                <td><?= $value['email'] ?></td>
                <td><a href="<?= BASE_URL ?>/home/show?<?=$value['id']?>" class="btn btn-primary me-2">Lihat</a><a href="<?=BASE_URL?>/edit?id=<?= $value["id"] ?>" class="btn btn-warning text-white me-2">Edit</a><a href="#" class="btn btn-danger">Hapus</a></td>
              
              </tr>
            <?php } var_dump($data);?>
          </tbody>
        </table>
      </div>
    </main>

