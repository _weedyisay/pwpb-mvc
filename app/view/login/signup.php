<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.101.0">
    <title></title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/sign-in/">

    

    

<link href="<?=BASE_URL?>/assets/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .b-example-divider {
        height: 3rem;
        background-color: rgba(0, 0, 0, .1);
        border: solid rgba(0, 0, 0, .15);
        border-width: 1px 0;
        box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
      }

      .b-example-vr {
        flex-shrink: 0;
        width: 1.5rem;
        height: 100vh;
      }

      .bi {
        vertical-align: -.125em;
        fill: currentColor;
      }

      .nav-scroller {
        position: relative;
        z-index: 2;
        height: 2.75rem;
        overflow-y: hidden;
      }

      .nav-scroller .nav {
        display: flex;
        flex-wrap: nowrap;
        padding-bottom: 1rem;
        margin-top: -1px;
        overflow-x: auto;
        text-align: center;
        white-space: nowrap;
        -webkit-overflow-scrolling: touch;
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="<?= BASE_URL ?>/assets/css/style.css" rel="stylesheet">
  </head>
  <body class="text-center">
    
<main class="form-signin w-100 m-auto bg-white rounded shadow-lg">
  <div class="row">
      <div class="col-lg-6">
        <?php Flasher::flash(); ?>
      </div>
    </div>
  <form method="post" action="<?=BASE_URL?>/login/store">
  <img class="mb-4" src="https://getbootstrap.com/docs/5.2/assets/brand/bootstrap-logo.svg" alt="" width="72" height="57" />
    <h1 class="h3 mb-3 fw-normal">Please Sign Up</h1>
    <?php if(isset($data['msg']) && $data['msg'] == 'exist_error'){ ?>
      <div class="alert alert-danger" role="alert">
        Email or Username already taken
      </div>
    <?php } elseif($data['msg'] != ''  ) {?>
      <div class="alert alert-danger" role="alert">
        <?= $data['msg'] ?>
      </div>
    <?php } ?>

    <div class="form-floating">
      <input type="text" class="form-control up" id="username" name="username" placeholder="username" required>
      <label for="username">Username</label>
    </div>
    <div class="form-floating">
      <input type="text" class="form-control" id="first_name" name="first_name" placeholder="first name" required>
      <label for="first_name">First Name</label>
    </div>
    <div class="form-floating">
      <input type="text" class="form-control" id="last_name" name="last_name" placeholder="last name" required>
      <label for="last_name">Last Name</label>
    </div>
    <div class="form-floating">
      <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" required>
      <label for="email">Email Address</label>
    </div>

    <div class="form-floating">
      <input type="password" class="form-control" id="password" name="password" placeholder="password" required>
      <label for="password">Password</label>
    </div>
    <div class="form-floating">
      <input type="password" class="form-control konfirmasi-password down" id="konfirm_password" name="konfirm_password" placeholder="Password" required>
      <label for="konfirm_password">Konfirmasi Password</label>
    </div>




    <button class="w-100 btn btn-lg btn-primary" type="submit">Sign up</button>

  </form>
  <div class="form-signin-footer">Already have an account?<a href="<?=BASE_URL?>/login/signin"> Login here</a></div>
</main>


    
  </body>
</html>
