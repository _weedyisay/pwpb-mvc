<?php
function url($path){
    return BASE_URL . "/". trim($path, "/");
}

function redirect($string) {
    header("Location: " . BASE_URL . "/$string");
    exit;
}


?>